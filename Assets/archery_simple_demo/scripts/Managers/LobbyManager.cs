using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.Events;
using Unity.Netcode.Transports.UTP;
using UnityEngine.SceneManagement;
using TMPro;

public class LobbyManager : MonoBehaviour
{
     // Singleton
     public static LobbyManager _instance;
     public static LobbyManager Instance => _instance;

     private string _lobbyId;

     private RelayHostData _hostData;
     private RelayJoinData _joinData;

     public TMP_InputField createInput;
     public TMP_InputField joinInput;


     // Setup events

     // Notify state update
     public UnityAction<string> UpdateState;
     // Notify Match found
     public UnityAction MatchFound;

     private void Awake()
     {
          // Just a basic singleton
          if (_instance is null)
          {
               _instance = this;
               return;
          }

          Destroy(this);
     }

     async void Start()
     {
          // Initialize unity services
          await UnityServices.InitializeAsync();

          // Setup events listeners
          SetupEvents();

          // Unity Login
          await SignInAnonymouslyAsync();

          // Subscribe to NetworkManager events
          NetworkManager.Singleton.OnClientConnectedCallback += ClientConnected;
     }

     #region Network events

     private void ClientConnected(ulong id)
     {
          // Player with id connected to our session

          Debug.Log("Connected player with id: " + id);

          UpdateState?.Invoke("Player found!");
          MatchFound?.Invoke();
     }

     #endregion

     #region UnityLogin

     void SetupEvents()
     {
          AuthenticationService.Instance.SignedIn += () =>
          {
               // Shows how to get a playerID
               Debug.Log($"PlayerID: {AuthenticationService.Instance.PlayerId}");

               // Shows how to get an access token
               Debug.Log($"Access Token: {AuthenticationService.Instance.AccessToken}");
          };

          AuthenticationService.Instance.SignInFailed += (err) =>
          {
               Debug.LogError(err);
          };

          AuthenticationService.Instance.SignedOut += () =>
          {
               Debug.Log("Player signed out.");
          };
     }

     async Task SignInAnonymouslyAsync()
     {
          try
          {
               await AuthenticationService.Instance.SignInAnonymouslyAsync();
               Debug.Log("Sign in anonymously succeeded!");
          }
          catch (Exception ex)
          {
               // Notify the player with the proper error message
               Debug.LogException(ex);
          }
     }

     #endregion

     #region Lobby

     public async void FindMatch()
     {

          Debug.Log("Looking for a lobby...");

          UpdateState?.Invoke("Looking for a match...");

          try
          {
               // Looking for a lobby

               // Add options to the matchmaking (mode, rank, etc..)
               QuickJoinLobbyOptions options = new QuickJoinLobbyOptions();

               // Quick-join a random lobby
               Lobby lobby = await Lobbies.Instance.QuickJoinLobbyAsync(options);

               Debug.Log("Joined lobby: " + lobby.Id);
               Debug.Log("Lobby Players: " + lobby.Players.Count);

               // Retrieve the Relay code previously set in the create match
               string joinCode = lobby.Data["joinCode"].Value;

               Debug.Log("Received code: " + joinCode);
               SceneManager.LoadScene("demo");
               Debug.Log("Scene changed");
               GameManager.joinCode = joinCode;
          }
          catch (LobbyServiceException e)
          {
               // If we don't find any lobby, let's create a new one
               Debug.Log("Cannot find a lobby: " + e);
               CreateMatch();
          }
     }

     public void CreateMatch()
     {
          SceneManager.LoadScene("demo");
          Debug.Log("Scene changed");
          GameManager.lobbyName = createInput.text;
     }

     IEnumerator HeartbeatLobbyCoroutine(string lobbyId, float waitTimeSeconds)
     {
          var delay = new WaitForSecondsRealtime(waitTimeSeconds);
          SceneManager.LoadScene("demo");
          while (true)
          {
               Lobbies.Instance.SendHeartbeatPingAsync(lobbyId);
               Debug.Log("Lobby Heartbit");
               yield return delay;

          }
     }


     public async void DeleteMatch()
     {
          try
          {
               Debug.Log(_lobbyId);
               await Lobbies.Instance.DeleteLobbyAsync(_lobbyId);
               NetworkManager.Singleton.Shutdown(); // Bug another user 
          }
          catch (LobbyServiceException e)
          {
               Debug.Log(e);
          }
     }
     private void OnDestroy()
     {
          // We need to delete the lobby when we're not using it
          Lobbies.Instance.DeleteLobbyAsync(_lobbyId);
     }

     #endregion

     /// <summary>
     /// RelayHostData represents the necessary informations
     /// for a Host to host a game on a Relay
     /// </summary>
     public struct RelayHostData
     {
          public string JoinCode;
          public string IPv4Address;
          public ushort Port;
          public Guid AllocationID;
          public byte[] AllocationIDBytes;
          public byte[] ConnectionData;
          public byte[] Key;
     }

     /// <summary>
     /// RelayHostData represents the necessary informations
     /// for a Host to host a game on a Relay
     /// </summary>
     public struct RelayJoinData
     {
          public string JoinCode;
          public string IPv4Address;
          public ushort Port;
          public Guid AllocationID;
          public byte[] AllocationIDBytes;
          public byte[] ConnectionData;
          public byte[] HostConnectionData;
          public byte[] Key;
     }
}